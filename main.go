package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

type paramsStruct struct {
	int1  int
	int2  int
	limit int
	str1  string
	str2  string
}

type responseStruct struct {
	Result []string `json:"result"`
}

const (
	defaultInt1  = 3
	defaultInt2  = 5
	defaultLimit = 100
	defaultStr1  = "fizz"
	defaultStr2  = "buzz"
)

func parseRequestParams(r *http.Request) (obj paramsStruct, err error) {
	vars := r.URL.Query()

	int1 := vars.Get("int1")
	if int1 == "" {
		obj.int1 = defaultInt1
	} else {
		obj.int1, err = strconv.Atoi(int1)
		if err != nil {
			return obj, fmt.Errorf("Unable to parse int1 value %s not an integer", int1)
		}
	}

	int2 := vars.Get("int2")
	if int2 == "" {
		obj.int2 = defaultInt2
	} else {
		obj.int2, err = strconv.Atoi(vars.Get("int2"))
		if err != nil {
			return obj, fmt.Errorf("Unable to parse int2 value %s not an integer", int2)
		}
	}

	limit := vars.Get("limit")
	if limit == "" {
		obj.limit = defaultLimit
	} else {
		obj.limit, err = strconv.Atoi(vars.Get("limit"))
		if err != nil {
			return obj, fmt.Errorf("Unable to parse limit value %s not an integer", limit)
		}
	}

	obj.str1 = vars.Get("str1")
	if obj.str1 == "" {
		obj.str1 = defaultStr1
	}

	obj.str2 = vars.Get("str2")
	if obj.str2 == "" {
		obj.str2 = defaultStr2
	}

	return obj, nil
}

func fizzBuzz(w http.ResponseWriter, r *http.Request) {
	params, err := parseRequestParams(r)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	var arr []string
	for i := 1; i <= params.limit; i++ {
		result := ""
		if i%params.int1 == 0 {
			result += params.str1
		}
		if i%params.int2 == 0 {
			result += params.str2
		}
		if result == "" {
			result = strconv.Itoa(i)
		}
		arr = append(arr, result)
	}
	res := &responseStruct{
		Result: arr}
	resMarshal, _ := json.Marshal(res)
	fmt.Println(string(resMarshal))
	json.NewEncoder(w).Encode(res)
}

func main() {
	var router = mux.NewRouter()
	router.HandleFunc("/fizzbuzz", fizzBuzz).Methods("GET")

	fmt.Println("Running server!")
	log.Fatal(http.ListenAndServe(":3000", router))
}
