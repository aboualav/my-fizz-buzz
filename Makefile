GOCMD=go
GOBUILD=$(GOCMD) build
GOCLEAN=$(GOCMD) clean
GOGET=$(GOCMD) get
BINARY_NAME=my-fizz-buzz

all: deps build

build:
	$(GOBUILD) -v .

clean:
	go clean -i .
	rm -f $(BINARY_NAME)

deps:
	$(GOGET) github.com/golang/dep/cmd/dep
	dep ensure

help:
	@echo "make: compile packages and dependencies"
	@echo "make clean: remove object files and cached files"
	@echo "make deps: get the deployment tools"